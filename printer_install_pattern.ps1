##############################################################################
$name_port=""
$printer_IP=""
$printer_name=""
$printer_driver=""

Add-PrinterPort -Name $name_port -PrinterHostAddress $printer_IP
Add-PrinterDriver -Name $printer_driver
Add-Printer -Name $printer_name -DriverName $printer_driver -Port $name_port


